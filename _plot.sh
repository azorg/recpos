#! /bin/sh

./_make.sh
./recpos_test > data.txt 2>err.txt

#gnuplot <<EOF
gnuplot -persist <<EOF
set grid
#unset border
plot "data.txt" using (\$1):(\$2) title "X,Y" with points ls 2
#pause -1
EOF


